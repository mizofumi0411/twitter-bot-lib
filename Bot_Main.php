<?php
	###################
	# Bot_Main クラス #
	###################

	// ライブラリの読み込み
	require_once("lib/UltimateOAuth.php");
	require_once("lib/TwitterOAuth.php");

	// みぞ氏製作ライブラリの読み込み
	require_once("geso/PDO_Wrapper.php");
	require_once("geso/Rest_API.php");

	// BotのOAuth認証データの読み込み
	include_once("Bot_Auth.php");

	// Bot_Main class の 定義
	class Bot_Main
	{
		##############
		# メンバ変数 #
		##############

		# ユーザー名を格納する変数
		var $_USER_;
		# UltimateOAuthオブジェクトを格納する変数
		var $_UO_;
		# TwitterOAuthオブジェクトを格納する変数
		var $_TO_;

		// TwitterAPI オブジェクトを格納する変数
		var $_REST_;

		# バージョン変数
		var $_VERSION_ = "0.0.1";

		// コンストラクタ (初期化用メソッド)
		function __construct()
		{
			$this -> _USER_ = $SCREEN_NAME;

			// UltimateOAuth オブジェクトの生成
			$this -> _UO_ = new UltimateOAuth($CONSUMER_KEY,$CONSUMER_SECRET,$ACCESS_TOKEN,$ACCESS_TOKEN_SECRET);

			// TwitterOAuth オブジェクトの生成
			$this -> _TO_ = new TwitterOAuth($CONSUMER_KEY,$CONSUMER_SECRET,$ACCESS_TOKEN,$ACCESS_TOKEN_SECRET);

			// TwitterAPI オブジェクトの生成
			$this -> _REST_ = new TwitterAPI($CONSUMER_KEY,$CONSUMER_SECRET,$ACCESS_TOKEN,$ACCESS_TOKEN_SECRET);
		}

		// リクエスト送信メソッド
		function Request($_URL_, $_METHOD_ = "POST", $_FORMAT_ = array())
		{
			$_REQUEST_ = $this -> _TO_ -> OAuthRequest("http://api.twitter.com/1.1/".$_URL_,$_METHOD_,$_FORMAT_);

			if($_REQUEST_)
			{
				$_RESULT = $_REQUEST_;
			}
			else
			{
				$_RESULT_ = NULL;
			}

			return $_RESULT_;
		}

		// データをロードするメソッド
		function Data_Load($_TYPE_)
		{
			# あとでMySQLとかSQLiteに書き換えたほうがいいかも #

			$_DAT_ = $this -> _USER_."_".$_TYPE_.".dat";

			if(!file_exists($_DAT_))
			{
				touch($_DAT_);
				chmod($_DAT_,0666);
				return null;
			}

			return file($_DAT_);
		}

		function Data_Save($_TYPE_,$_DATA_)
		{
			$_DAT_ = $this -> _USER_."_".$_TYPE_.".dat";

			if(!file_exists($_DAT_))
			{
				touch($_DAT_);
				chmod($_DAT_,0666);
			}

			$_OPEN_DAT_ = fopen($_DAT_,"w");
			flock($_OPEN_DAT_,LOCK_EX);
			fputs($_OPEN_DAT_,$_DATA_);
			flock($_OPEN_DAT_,LOCK_UN);
			fclose($_OPEN_DAT_);
		}

		// ツイートを送信するメソッド
		function Post($_STATUS_,$_REPLY_ = NULL)
		{
			$_TWEET_ = array("status"=>$_STATUS_);

			if($_REPLY_)
			{
				$_TWEET_['in_reply_to_status_id'] = $_REPLY_;
			}

			# Request() 使用
			$_REQUEST_ = $this -> Request("statues/update.json","POST",$_TWEET_);
			# REST API WRAPPER 使用
			# $_REQUEST_ = $this -> _REST_ -> UpdateStatus($_TWEET_);

			if(!$_REQUEST_)
			{
				die('Post(): $_REQUEST_ is NULL');
			}

			$_CODE_ = $_REQUEST_ -> Code;
			$_RESULT_ = json_decode($_REQUEST_ -> Body);

			if(($_CODE_ == "200" || $_CODE_ == "403") && $_REPLY_)
			{
				$this -> Data_Save("Since",$_REPLY_);
				return;
			}

			if($_RESULT_ -> error)
			{
				die($_CODE_.", ".$_RESULT_ -> error);
			}
		}

		function Follow($_USER_ID_, $_FLAG_ = true)
		{
			$_RESULT_ = "OK";

			$_KEY_ = ctype_digit($_USER_ID_)?"user_id":"screen_name";
			$_HOMU_ = array($_KEY_ => $_USER_ID_);

			if($_FLAG_)
			{
				$_HOMU_['follow'] = "true";
			}

			$_REQUEST_ = $this -> Request("friendships/".($_FLAG_?"create":"destroy").".json",$_FLAG_?"POST":"DELETE",$_HOMU_);

			if($_REQUEST_)
			{
				if($_REQUEST_ -> Code != "200")
				{
					$_RESULT_ = "ERROR";
				}

				$_RESULT_ = json_decode($_REQUEST_ -> Body);

				if($_RESULT_ -> error)
				{
					$_RESULT_ = "already";
				}
			}
			else
			{
				$_RESULT_ = "error";
			}

			return $_RESULT_;
		}

		// ふぁぼるメソッド
		function addFavorite($_STATUS_ID_)
		{
			$_REQUEST_ = $this -> Request("favorites/create.json","POST",array("id" => $_STATUS_ID_,"include_entities" => "false"));

			if(!$_REQUEST_)
			{
				die('Favorite(); $_REQUEST_ is NULL');
			}
			if($_REQUEST_ -> Code != "200")
			{
				die("Error: ".$_REQUEST_ -> Code);
			}
		}

		// ツイ消しメソッド。自分以外のツイートは消せない(当たり前)
		function Delete($_STATUS_ID_)
		{
			$req = $this->Request("statuses/destroy/".$_STATUS_ID_.".json","POST");
			if($_REQUEST_)
			{
				if($_REQUEST_->Code != "200")
				{
					die("Error: ".$_REQUEST_ -> Code);
				}

				$_RESULT_ = json_decode($_REQUEST_->Body);

				if($_RESULT_ -> error)
				{
					die($_RESULT_ -> error);
				}
			}
			else
			{
				die('Delete(): $_REQUEST_ is NULL');
			}
		}

		// RTFメソッド
		function ReTweet($_STATUS_ID_)
		{
			$_REQUEST_ = $this->Request("statuses/retweet/".$_STATUS_ID_.".json","POST");

			if($_REQUEST_)
			{
				if($_REQUEST_->Code != "200")
				{
					die("Error: ".$_REQUEST_ -> Code);
				}

				$_RESULT_ = json_decode($_REQUEST_ -> Body);

				if($_RESULT_ -> error)
				{
					die($_RESULT_->error);
				}
			}
			else
			{
				die('ReTweet(): $_REQUEST_ is NULL');
			}
		}

		// DM送信メソッド。
		## $_USER_ID : ユーザーナンバー or ユーザーネーム(screen_name)
		## $_TEXT_   : 本文
		function DirectMessage($_USER_ID,$_TEXT_)
		{
			$_KEY_ = ctype_digit($_USER_ID)?"user_id":"screen_name";

			$_REQUEST_ = $this->Request("direct_messages/new.json","POST",array($_KEY_=>$_USER_ID,"text"=>$_TEXT_));

			if($_REQUEST_)
			{
				if($_REQUEST_ -> Code != "200")
				{
					die("Error: ".$_REQUEST_ -> Code);
				}

				$_RESULT_ = json_decode($_REQUEST_ -> Body);

				if($_RESULT_->error)
				{
					die($_RESULT_ -> error);
				}
			}
			else
			{
				die('DirectMessage(): $_REQUEST_ is NULL');
			}
		}

		function End($_SINCE_ID_)
		{
			$this -> Data_Save("Since",$_SINCE_ID_);

			echo "Normal Termination: ".sprintf("%0.4f",array_sum(explode(" ",microtime())) - $this->times)." sec, ".date("H:i:s");
		}

		// 配列$_NOEL_からランダムに一つ取り出す
		function getRandom($_NOEL_)
		{
			if(!is_array($_NOEL_))
			{
				return $_NOEL_;
			}

			$_RANDOM_ = array_rand($_NOEL_,1);

			return $_NOEL_[$_RANDOM_];
		}

		// TLとかを取得するメソッド
		function Get_TimeLine($_TYPE_, $_SINCE_ID_ = NULL, $_COUNT_ = 30)
		{
			########
			# MEMO ####################################################################
			#		$_TYPE_ に home_timeline や mentions_timeline などを入れる。
			#		詳しくはAPI仕様書を見る。
			#
			#		$_SINCE_ID_ はツイートのID。
			#		$_SINCE_ID_ で指定したツイートのIDより後のツイートを取得するようにさせる。
			#
			#		$_COUNT_ は一度にツイートをどれだけ取得するか。最大200。
			###########################################################################

			$_MADOKA_ = array("count"=>$_COUNT_);

			if($_SINCE_ID_)
			{
				$_MADOKA_['since_id'] = $_SINCE_ID_;
			}

			$_REQUEST_ = $this -> Request("statues/".$_TYPE_.".json","GET",$_MADOKA_);

			if($_REQUEST_)
			{
				if($_REQUEST_ -> Code != "200")
				{
					die("Error: ".$_REQUEST_ -> Code);
				}

				$_RESULT_ = json_decode($_REQUEST_ -> Body);
			}
			else
			{
				die('Get_Timeline(): $_REQUEST_ is NULL');
			}

			return array_reverse($_RESULT_);
		}

		##############################
		# REST_API LIB  使用メソッド #
		##############################

		// ホームタイムラインを取得するメソッド
		function GetHomeTimeline($_COUNT_,$_SINCE_ID_,$_MAX_ID_)
		{
			$_HOME_TL_ = $this -> _REST_ -> GetHomeTimeline($_COUNT_,$_SINCE_ID_,$_MAX_ID_);

			return $_HOME_TL_;
		}
		// メンションを取得するメソッド
		function GetMentionsTimeline($_COUNT_ = 30,$_SINCE_ID_ = -1, $_MAX_ID_ = -1)
		{
			$_MENTIONS_ = $this -> _REST_ -> GetMentionsTimeline($_COUNT_,$_SINCE_ID_,$_MAX_ID_);

			return $_MENTIONS_;
		}

		##############################
		# Responseクラス使用メソッド #
		##############################

		function Speaks($_INPUT_)
		{
			return $this -> _RESPONDER_ -> Response($_INPUT_);
		}

		function ResponderName()
		{
			return $this -> _RESPONDER_ -> Name();
		}
	}

	class Bot_Debug extends Bot_Main
	{
		function Debug_Post($_TWEET_)
		{
			$_TWEET_ = array();

			$_DEBUG_ = $this -> _UO_ -> POST_statuses_update(array('status'=>$_TWEET_));

			return $_DEBUG_;
		}

		//文字コードをSJISに変換して出力するメソッド
		function Debug_EncodeToSJIS($_TEXT_)
		{
			print mb_convert_encoding($_TEXT_, "SJIS", "auto")."\n";
		}
	}
?>