<?php

class DB
{
  private $hostname;
	private $db_username;
	private $db_password;
	private $dbName;

	public $dbh = NULL;

	public function __construct($hostname,$db_username,$db_password,$dbName)
	{
		try
		{
			$this->dbh = new PDO("mysql:host=$hostname;dbname=$dbName", $this->db_username=$db_username, $this->db_password=$db_password);
		}
		catch(PDOException $e)
		{
			echo __LINE__.$e->getMessage();
		}
	}

	public function __destruct()
	{
		$this->dbh = NULL;
	}

	public function runQuery($sql)
	{
		try
		{
			$count = $this->dbh->exec($sql) or $count = $this->dbh->errorInfo();
		}
		catch(PDOException $e)
		{
			echo __LINE__.$e->getMessage();
		}
	}

	public function getQuery($sql)
	{
		$stmt = $this->dbh->query($sql);

	    $stmt->setFetchMode(PDO::FETCH_ASSOC);

		return $stmt;
	}

}
?>